import com.google.common.base.Splitter;
import com.google.common.collect.Lists;

import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        System.out.println("Please give us few words:");

        String name = scanner.nextLine();

        List<String> ls = Lists.newArrayList(
                Splitter.on(' ')
                        .trimResults()
                        .omitEmptyStrings()
                        .split(name)
        );

        System.out.printf("You've entered %s words! Thanks!", ls.size());

    }
}
